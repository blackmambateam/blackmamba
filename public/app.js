
var app = angular.module('Blackmamba',
	[ 
	 	'ui.router',
	 	'ngMaterial',
	 	'BlackmambaControllers',
	 	'BlackmambaServices'
	]).config(function($stateProvider, $urlRouterProvider) {
	    
	    $urlRouterProvider.otherwise('/home');
	    
	    //http://stackoverflow.com/questions/16363426/ui-router-why-parent-states-must-be-abstract
	    $stateProvider
		    .state('home', {
		        url: '/home',
		        views: {
		            // the main template will be placed here (relatively named)
		            '': { templateUrl: './Home/_HomeView.html' },
	
		            // the child views will be defined here (absolutely named)
		            'map@home': { templateUrl: './Map/_MapView.html' },
	
		            // for column two, we'll define a separate controller 
		            'input@home': { 
		            	templateUrl: './Input/_InputView.html',
		            	controller: 'InputCtrl'
		            }
		        }
		    });
	});

var appControllers = angular.module('BlackmambaControllers', []);

var appServices = angular.module('BlackmambaServices', []);